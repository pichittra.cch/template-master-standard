import { SharedModule } from './../../shared.module';
import { ProfileComponent } from "./profile.component";
import { CommonModule } from "@angular/common";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
	declarations: [ProfileComponent],
	providers: [],
	imports: [
		CommonModule,
		SharedModule,
		RouterModule
	],
	exports: [
		ProfileComponent
	]
})
export class ProfileModule {}