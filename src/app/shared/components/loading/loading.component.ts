import { Component, OnInit, Input } from '@angular/core';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
@Component({
	selector: 'loading',
	templateUrl: './loading.component.html',
	styleUrls: ['./loading.component.scss']
})

export class LoadingComponent implements OnInit {
	constructor(

	) {}

	ngOnInit() {}

	@Input("show") loading: boolean = false;

	public config = { 
		animationType: ngxLoadingAnimationTypes.threeBounce,
		backdropBackgroundColour: 'rrgba(0,0,0,0.4)', 
		backdropBorderRadius: '10px',
		primaryColour: '#F26D23', 
		secondaryColour: '#ff1744', 
		tertiaryColour: '#aeea00' 
	}
}
