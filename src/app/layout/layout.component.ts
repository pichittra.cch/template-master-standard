import { GetMenu } from './../actions/menu.actions';
import {
  Component,
  ViewEncapsulation,
  ElementRef,
  OnInit,
  HostBinding
} from "@angular/core";
import { Subject } from "rxjs";
import { GetProfile } from "../actions/user.actions";
import { Store } from "@ngxs/store";
import { finalize, takeUntil } from "rxjs/operators";
// import { GlobalState } from "../app.state";

// import { ConfigService } from "../shared/services/config/config.service";
@Component({
  selector: "app-layout",
  templateUrl: "./layout.component.html",
  styleUrls: ["./layout.component.scss"],
  encapsulation: ViewEncapsulation.Emulated
})
export class LayoutComponent implements OnInit {
  constructor(
    private store: Store
  ) {}

  ngOnInit() {
    // this.getProfile();
    // this.getMenu();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  /////////////////// Config ///////////////////

  ///////////////// End Config /////////////////
  ///////////////// Variable /////////////////
  public profile;
  public menuList;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  public getProfile() {
   
    this.store.dispatch(new GetProfile())
    .pipe(
      finalize(() => {
   
      }),
      takeUntil(this.unsubscribeAll)
    )
    .subscribe(
      value => {
        this.profile = value.userState.profile.result;
      },
      error => {
        console.log("Error:", error)
      }
    );

  }

  public getMenu() {
   
    this.store.dispatch(new GetMenu())
    .pipe(
      finalize(() => {
   
      }),
      takeUntil(this.unsubscribeAll)
    )
    .subscribe(
      value => {
        this.menuList = value.menuState.menu.result;
      },
      error => {
        console.log("Error:", error)
      }
    );

  }
}
