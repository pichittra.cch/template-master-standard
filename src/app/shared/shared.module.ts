// Angular
// https://angular.io/
import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from '@angular/flex-layout';

// Angular Material
// https://material.angular.io/
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatButtonToggleModule,
	MatCardModule,
	MatCheckboxModule,
	MatChipsModule,
	MatDatepickerModule,
	MatDialogModule,
	MatExpansionModule,
	MatGridListModule,
	MatIconModule,
	MatInputModule,
	MatListModule,
	MatMenuModule,
	MatNativeDateModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatRippleModule,
	MatSelectModule,
	MatSidenavModule,
	MatSliderModule,
	MatSlideToggleModule,
	MatSnackBarModule,
	MatTabsModule,
	MatToolbarModule,
	MatTooltipModule
} from "@angular/material";
// import { NguUtilityModule } from "ngu-utility/dist";
import { TranslateModule } from "@ngx-translate/core";
import { StorageServiceModule } from 'ngx-webstorage-service';

// UI Shared Components
import { AppBackdropComponent } from "./components/app_backdrop/app_backdrop.component";
import { LoadingModule } from "./components/loading/loading.module";
import { EcoFabSpeedDialModule } from '@ecodev/fab-speed-dial';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { MatDatePickerTHModule } from './components/mat-date-picker';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatAutocompleteModule,
		MatButtonModule,
		MatButtonToggleModule,
		MatCardModule,
		MatCheckboxModule,
		MatChipsModule,
		MatDatepickerModule,
		MatDialogModule,
		MatExpansionModule,
		MatGridListModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatMenuModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatProgressSpinnerModule,
		MatRadioModule,
		MatRippleModule,
		MatSelectModule,
		MatSidenavModule,
		MatSliderModule,
		MatSlideToggleModule,
		MatSnackBarModule,
		MatTabsModule,
		MatToolbarModule,
		MatTooltipModule,
		MatDatePickerTHModule,
		// NguUtilityModule,
		NgbModule.forRoot(),
		FlexLayoutModule,
		TranslateModule,
		StorageServiceModule,
    LoadingModule,
	EcoFabSpeedDialModule,
	NgxUiLoaderModule
	],
	declarations: [
		AppBackdropComponent
	],
	exports: [
		CommonModule,
		FormsModule,
		MatAutocompleteModule,
		MatButtonModule,
		MatButtonToggleModule,
		MatCardModule,
		MatCheckboxModule,
		MatChipsModule,
		MatDatepickerModule,
		MatDialogModule,
		MatExpansionModule,
		MatGridListModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatMenuModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatProgressSpinnerModule,
		MatRadioModule,
		MatRippleModule,
		MatSelectModule,
		MatSidenavModule,
		MatSliderModule,
		MatSlideToggleModule,
		MatSnackBarModule,
		MatTabsModule,
		MatToolbarModule,
		MatTooltipModule,
		// NguUtilityModule,
		AppBackdropComponent,
		ReactiveFormsModule,
		NgbModule,
		FlexLayoutModule,
		TranslateModule,
		StorageServiceModule,
		NgxUiLoaderModule,
		MatDatePickerTHModule,
    LoadingModule,
    EcoFabSpeedDialModule
	]
})
export class SharedModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: SharedModule
		};
	}
}
