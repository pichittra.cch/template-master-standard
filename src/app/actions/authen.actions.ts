export class GetHealth {
    static readonly type = '[AUTHEN] Get Health'
}

export class AuthLogin {
    static readonly type = '[AUTHEN] Authen login'

    constructor(public payload: any) {}
}

export class GetAccessToken {
    static readonly type = '[AUTHEN] Get Access Token'

    constructor(public payload: any) {}
}