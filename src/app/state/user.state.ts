import { ResultData } from './../models/result.data';
import { State, Action, StateContext } from '@ngxs/store';
import { tap } from "rxjs/operators";
import { GetProfile } from '../actions/user.actions';
import { UserService } from '../services/user.service';


export interface UserStateModel {
    profile: ResultData
}

@State<UserStateModel>({
    name: "userState"
})


export class UserState {

    constructor(public service: UserService) {}

    @Action(GetProfile)
    getProfile(ctx: StateContext<UserStateModel>) {
        const state = ctx.getState();
        
        return this.service.getMe().pipe(
            tap(value => {
                ctx.patchState({ 
                    ...state,
                    profile: value
                });
            })
        )
    }
}