import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelperService } from '@auth0/angular-jwt';
// import { Idle } from 'idlejs/dist';
var _ = require('lodash');

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private cookieService: CookieService
    ) { }

    async canActivate() {

        let result = await this.checkToken();
        if(result){
            return true
        }

        this.router.navigate(['/login']);
        return false;
    }

    private checkToken(){
        const jwt = new JwtHelperService();

        const isLogin = this.cookieService.get('ba_ilg')
        if (isLogin === "true") {
            const refreshToken = this.cookieService.get('ba_rft')

            if(refreshToken !== ""){
                const isExpiredRefreshToken = jwt.isTokenExpired(refreshToken);

                if(!isExpiredRefreshToken){
                    return true;
                }
            }
            return false;
        }

        return false;
    }

    // private  idleLogOut() {
        
        
    //     if(this.idle === null){
    //         this.idle = new Idle()
    //         .whenNotInteractive()
    //         .within(15)//.within(10, 1000)
    //         .do(async () => {
    //             this.idle = null

    //             let i = sessionStorage.length;
    //             while (i--) {
    //                 let key = sessionStorage.key(i);
    //                 sessionStorage.removeItem(key);
    //             }
    //             await this.cookieService.deleteAll("/")
	// 	        await this.router.navigate(['/login'])
    //         })
    //         .start();
            
    //     }
            
    // }
}