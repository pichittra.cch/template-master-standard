import { NgModule } from "@angular/core";
import { BrowserModule, Title } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  FormsModule,
  ReactiveFormsModule
} from "@angular/forms";
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from "@angular/common/http";
import { AppRoutingModule } from "./app.routing.module";
import { AppComponent } from "./app.component";
import { AppState, InternalStateType } from "./app.service";
import { GlobalState } from "./app.state";
import { PLATFORM_ID, APP_ID, Inject } from "@angular/core";
import { isPlatformBrowser } from '@angular/common';
import {
  HammerGestureConfig,
  HAMMER_GESTURE_CONFIG
} from "@angular/platform-browser";
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { StorageServiceModule } from "ngx-webstorage-service";
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { CookieService } from "ngx-cookie-service";
import { TokenInterceptor } from "./shared/interceptors/token.interceptor";
import { AuthenState } from "./state/authen.state";
import { NgxsModule } from "@ngxs/store";
import { SharedModule } from './shared/shared.module';

export class HammerConfig extends HammerGestureConfig {
  overrides = <any>{
    swipe: { velocity: 0.4, threshold: 20 }
  };
}
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

// Application wide providers
const APP_PROVIDERS = [
  AppState,
  GlobalState,
  Title,
  CookieService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  },
  // { provide: LocationStrategy, useClass: HashLocationStrategy },
  { provide: HAMMER_GESTURE_CONFIG, useClass: HammerConfig }
];

export type StoreType = {
  state: InternalStateType;
  restoreInputValues: () => void;
  disposeOldHosts: () => void;
};

@NgModule({
  imports: [
    BrowserModule.withServerTransition({ appId: "life-and-living" }),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    StorageServiceModule,
    SharedModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }
    }),
    NgxsModule.forRoot([
      AuthenState
    ]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
  ],
  declarations: [AppComponent],
  providers: [APP_PROVIDERS],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    public appState: AppState,
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
    const platform = isPlatformBrowser(platformId)
      ? "in the browser" : "on the server";
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
