import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';


const routes: Routes = [
  {
    path: "",
    // canActivate: [AuthGuard],
    loadChildren: "./layout/layout.module#LayoutModule"
  },

  { path: "login", loadChildren: "./pages/authen/login/login.module#LoginModule" },
  { path: "logout", loadChildren: "./pages/authen/logout/logout.module#LogoutModule" },
  { path: 'not-found', loadChildren: './pages/error/404/404.module#Error404Module' },

  { path: "**", redirectTo: "not-found" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
	exports: [RouterModule]
})
export class AppRoutingModule {}
