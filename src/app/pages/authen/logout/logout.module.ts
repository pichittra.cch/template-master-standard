import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LogoutComponent } from './logout.component';
import { SharedModule } from '../../../shared/shared.module';
import { NgxsModule } from '@ngxs/store';
import { AuthenState } from '../../../state/authen.state';

const ROUTE = [
    { path: "", component: LogoutComponent }
];

@NgModule({
    providers: [],
    declarations: [
        LogoutComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        NgxsModule.forRoot([
            AuthenState
        ]),
        RouterModule.forChild(ROUTE)
    ]

})
export class LogoutModule { }