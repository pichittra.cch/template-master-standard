import { Component, OnInit, Input } from '@angular/core';
@Component({
	selector: 'profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {
	isProfileVisible: boolean = true;
	isProfileSubmenuVisible: boolean = false;
	profile: any;
	
	constructor(

	) {}

	ngOnInit() {}

	@Input("data") data;

}
