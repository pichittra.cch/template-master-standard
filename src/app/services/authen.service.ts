import { IAuth } from './../shared/services/config/rest-url.config.service';
import { ResultData } from './../models/result.data';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { RestUrlConfigService } from '../shared/services/config/rest-url.config.service';


@Injectable({
  providedIn: 'root'
})
export class AuthenService {
  constructor(
    private http: HttpClient,
    private restUrl: RestUrlConfigService
  ) { 
    this.urlServ = this.restUrl.auth;
  }

  public urlServ: IAuth;

  getHealth = (): Observable<any> => {

    return this.http
      .get<ResultData>(this.urlServ.getHealth);
  };

  authLogin = (params): Observable<any> => {
    const data = {...params.payload}

    return this.http
      .post<ResultData>(this.urlServ.authLogin, data);
  };

  getAccessToken = (params): Observable<any> => {
    const data = {...params.payload}

    return this.http
      .post<ResultData>(this.urlServ.getAccessToken, data);
  };
  
}
