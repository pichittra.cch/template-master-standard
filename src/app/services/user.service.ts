import { IUser } from './../shared/services/config/rest-url.config.service';
import { ResultData } from './../models/result.data';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { RestUrlConfigService } from '../shared/services/config/rest-url.config.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private http: HttpClient,
    private restUrl: RestUrlConfigService
  ) { 
    this.urlServ = this.restUrl.user;
  }

  public urlServ: IUser;

  getMe = (): Observable<any> => {
    return this.http
      .get<ResultData>(this.urlServ.getMe);
  };
}
