import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { MatDatePickerTH } from "./mat-date-picker.component";
import { FocusDirective } from "./directives/my-date-picker.focus.directive";
import { MatInputModule, MatIconModule } from "@angular/material";


@NgModule({
    imports: [CommonModule, FormsModule, MatInputModule, MatIconModule],
    declarations: [MatDatePickerTH, FocusDirective],
    exports: [MatDatePickerTH, FocusDirective]
})
export class MatDatePickerTHModule {
}
