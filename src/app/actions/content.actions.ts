export class GetContent {
    static readonly type = '[Content] Get Content'
}

export class PostContent {
    static readonly type = '[Content] Post Content'

    constructor(public payload: any) {}
}

// export class AuthLogin {
//     static readonly type = '[AUTHEN] Authen login'

//     constructor(public payload: any) {}
// }

// export class GetAccessToken {
//     static readonly type = '[AUTHEN] Get Access Token'

//     constructor(public payload: any) {}
// }