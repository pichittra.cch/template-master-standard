import { MenuState } from './../state/menu.state';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LayoutRoutes } from "./layout.routes";
import { FormsModule } from "@angular/forms";
import { LayoutComponent } from "./layout.component";
import { LeftSidebarComponent } from "./left-sidebar/left-sidebar.component";
import { TopNavbarComponent } from "./top-navbar/top-navbar.component";
import { FooterComponent } from "./footer/footer.component";
// import { SearchComponent } from "./top-navbar/search/search.component";
// import { RightSidebarComponent } from "./right-sidebar/right-sidebar.component";
import { SharedModule } from "../shared/shared.module";
// import { ScrollbarDirective } from "../shared/directives/scrollbar.directive";
import { NavDropDownDirectives } from "../shared/directives/nav-dropdown.directive";
import { ProfileModule } from "../shared/components/profile/profile.module";
import { NgxsModule } from "@ngxs/store";
import { UserState } from "../state/user.state";
import { PerfectScrollbarConfigInterface, PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	suppressScrollX: true
};
@NgModule({
	declarations: [
		LayoutComponent,
		LeftSidebarComponent,
		TopNavbarComponent,
		// FooterComponent,
		// SearchComponent,
		// RightSidebarComponent,
		// ScrollbarDirective,
		NavDropDownDirectives
	],
	providers: [
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		}
	],
	imports: [
		LayoutRoutes,
		CommonModule,
		ProfileModule,
		SharedModule.forRoot(),
		PerfectScrollbarModule,
		NgxsModule.forRoot([
			UserState,
			MenuState
		]),
	]
})
export class LayoutModule {}
