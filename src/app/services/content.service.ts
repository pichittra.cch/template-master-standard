import { IContent } from './../shared/services/config/rest-url.config.service';
import { ResultData } from './../models/result.data';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { RestUrlConfigService } from '../shared/services/config/rest-url.config.service';


@Injectable({
  providedIn: 'root'
})
export class ContentService {
  constructor(
    private http: HttpClient,
    private restUrl: RestUrlConfigService
  ) { 
    this.urlServ = this.restUrl.content;
  }

  public urlServ: IContent;

  getContent = (): Observable<any> => {

    return this.http
      .get<ResultData>(this.urlServ.getContent);
  };

  postContent = (params): Observable<any> => {
    const data = {...params.payload}

    return this.http
      .post<ResultData>(this.urlServ.postContent, data);
  };

  // getAccessToken = (params): Observable<any> => {
  //   const data = {...params.payload}

  //   return this.http
  //     .post<ResultData>(this.urlServ.getAccessToken, data);
  // };
  
}
