import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { NgxsModule } from "@ngxs/store";
import { SharedModule } from '../../../../shared/shared.module';
import { JobConstructionViewComponent } from './job-construction-view.component';

const ROUTE = [
  { path: "", component: JobConstructionViewComponent }
];

@NgModule({
  declarations: [JobConstructionViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgxsModule.forRoot([
      
    ]),
    RouterModule.forChild(ROUTE)
  ]
})
export class JobConstructionViewModule {}
