import {
  Component,
  ViewEncapsulation,
  OnInit,
  Inject,
  ViewChild,
  TemplateRef
} from "@angular/core";
import { SESSION_STORAGE, StorageService, LOCAL_STORAGE } from "ngx-webstorage-service";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Subject } from "rxjs/Rx";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from '../../../../shared/services/config/config.service';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import * as _ from "lodash";

@Component({
  selector: ".job-construction-update-inner-wrapper",
  templateUrl: "./job-construction-update.component.html",
  styleUrls: ["./job-construction-update.component.scss"],
  encapsulation: ViewEncapsulation.Emulated
})

export class JobConstructionUpdateComponent implements OnInit {
  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  data = {
    code: "",
    name: "",
    detail: "",
    accountCode: "",
    budgetCode: "",
    workOrderType: "L",
    cert: true,
    active: true,
    constructionGroupId: "",
    constructionGroupCode: ""
  };
  drpConstructionGroup: any;

  id: string;
  drpWorkGroup: any;
  workModelName: any;
  tempWorkModel: any[] = [];
  indexWorkModel: any; //เก็บ id เพื่อใช้ตอน edit

  //////////////////Modal /////////////////////
  @ViewChild("modalWorkModel")
  public modalWorkModel: TemplateRef<any>;
  modalReferenceWorkModel: NgbModalRef;
  ///////////////// End Modal ////////////////////

  /* paggine */
  maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  maxSize: number = this.config.paging.MAXSIZE;

  submitValue: any = {
    result: true,
    resultMsg: ""
  };

  ValidateWorkModel: any = {
    result: true,
    resultMsg: ""
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store,
    public config: ConfigService,
    private modalService: NgbModal,
  ) { }


  ngOnInit() {

    this.id = this.route.snapshot.params["id"] || "0";
    this.getDrpConstructionGroupList()
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public getDrpConstructionGroupList() {
    this.drpConstructionGroup = [
      {
        id: '1',
        name: 'Finishing 1'
      },
      {
        id: '2',
        name: 'Finishing 2'
      },
      {
        id: '3',
        name: 'Finishing 3'
      }
    ]
    // this.loading = true
    // this.store.dispatch(new GetDrpConstructionGroup())
    //   .pipe(
    //     finalize(() => {
    //       this.loading = false;
    //     }),
    //     takeUntil(this.unsubscribeAll)
    //   )
    //   .subscribe(
    //     value => {
    //       this.drpConstructionGroup = value.jobConstructionState.ResultData.result;

    //       if (this.id != '0') {
    //         this.getData()
    //       }

    //     },
    //     error => {
    //       this.loading = false
    //     }
    //   );
  }

  public getData() {
    // const data = {
    //   jobId: this.id,
    // }

    // this.loading = true;
    // this.store.dispatch(new GetJobConstructionData(data))
    //   .pipe(
    //     finalize(() => {
    //       this.loading = false;
    //     }),
    //     takeUntil(this.unsubscribeAll)
    //   )
    //   .subscribe(
    //     value => {
    //       this.data = {...value.jobConstructionState.ResultData.result}
    //       this.tempWorkModel = value.jobConstructionState.ResultData.result.jobStyleList

    //       this.drpConstructionGroup.filter(item => {
    //         /////////////// หาชื่อกลุ่มงาน เพื่อเอารหัสกลุ่มงาน /////////////////
    //         if (this.data.constructionGroupId == item.id) {
    //           return this.data.constructionGroupCode = item.code
    //         }
    //       })
    //     },
    //     error => {
    //       this.loading = false
    //     }
    //   );
  }

  public selectedConstructionGroup() {
    this.drpConstructionGroup.filter(item => {
      /////////////// หาชื่อกลุ่มงาน เพื่อเอารหัสกลุ่มงาน /////////////////
      if (this.data.constructionGroupId == item.id) {
        return this.data.constructionGroupCode = item.code
      }
    })
  }

  public onSubmit() {
    this.router.navigate([`/master-setting/job-construction`]);
    // /* เอาแบบงานที่เพิ่งเพิ่มใหม่ (ไม่ได้มาจาก service) ออก */
    // let tempWorkModel = this.tempWorkModel.filter(item => {
    //   return item.flag != 3
    // })

    // const data = {
    //   id: this.id,
    //   code: this.data.code,
    //   name: this.data.name,
    //   detail: this.data.detail,
    //   accountCode: this.data.accountCode,
    //   budgetCode: this.data.budgetCode,
    //   workOrderType: this.data.workOrderType,
    //   cert: true,
    //   active: this.data.active,
    //   constructionGroupId: this.data.constructionGroupId,
    //   jobStyleList: tempWorkModel,
    //   createdBy: ""
    // }

    // if (this.data.constructionGroupId == "") {
    //   this.submitValue.result = false
    //   this.submitValue.resultMsg = "กรุณาเลือกกลุ่มงาน"
    // } else if (this.data.code == "") {
    //   this.submitValue.result = false
    //   this.submitValue.resultMsg = "กรุณากรอกชื่อรหัสงาน"
    // } else if (this.data.name == "") {
    //   this.submitValue.result = false
    //   this.submitValue.resultMsg = "กรุณากรอกชื่องาน"
    // } else {
    //   this.loading = true;
    //   this.store.dispatch(new PostJobConstructionUpdate(data))
    //     .pipe(
    //       finalize(() => {
    //         this.loading = false;
    //       }),
    //       takeUntil(this.unsubscribeAll)
    //     )
    //     .subscribe(
    //       value => {
    //         if (value.jobConstructionState.ResultData.result) {
    //           this.router.navigate([`/master-setting/job-construction`]);
    //           this.submitValue.result = true
    //         } else {
    //           this.submitValue.result = false
    //           this.submitValue.resultMsg = value.jobConstructionState.ResultData.message
    //         }
    //       },
    //       error => {
    //         this.loading = false
    //       }
    //     );
    // }
  }

  flag: number;
  joyStyleList: any[] = []
  /* modal แบบงาน */
  public openModalAddWorkModel(items, flag) {
    this.flag = flag
    this.indexWorkModel = undefined;
    this.workModelName = [];

    this.modalReferenceWorkModel = this.modalService.open(items, { size: 'lg' });
  }

  public confirmAddWorkModel() {

    /* เช็คชื่อซ้ำ */
    // let data = this.tempWorkModel.find(temp => this.workModelName.name == temp.name);
    let data = this.tempWorkModel.find(temp => this.workModelName == temp);

    if (!data) {

      this.ValidateWorkModel.result = true;
      this.modalReferenceWorkModel.close()

      /* เช็คว่า add(indexWorkModel == undefined) หรือ edit(indexWorkModel != undefined) */
      if (this.indexWorkModel == undefined) {
        //this.tempWorkModel.push(this.workModelName)
        this.tempWorkModel.push({
          name: this.workModelName,
          flag: this.flag
        })
      } else {
        //this.tempWorkModel[this.indexWorkModel] = this.workModelName
        this.tempWorkModel[this.indexWorkModel] = {
          id: this.tempWorkModel[this.indexWorkModel].id,
          name: this.workModelName,
          flag: this.flag
        }
      }

    } else {
      this.ValidateWorkModel.result = false;
      this.ValidateWorkModel.errMsg = "ชื่อแบบงานซ้ำ ไม่สามารถบันทึกข้อมูลได้";
    }
  }


  public editWorkModel(index, items, flag) {
    this.flag = flag
    /* นำ index ไปใช้ตอนกด confirm */
    this.indexWorkModel = index;
    /* เช็ค index ของ work model เพื่อใส่ค่าใน text box */
    this.workModelName = this.tempWorkModel[index].name
    this.modalReferenceWorkModel = this.modalService.open(items, { size: 'lg' });
  }

  public delWorkGroupModel(index, flag) {

    Swal({
      title: "คุณต้องการลบใช่หรือไม่",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: 'btn width-button',
      cancelButtonClass: 'btn btn-danger width-button',
      cancelButtonColor: '#d33',
      confirmButtonText: "บันทึก",
      cancelButtonText: "ยกเลิก"
    }).then((res) => {
      if (res.value) {
        this.flag = flag
        //confirm
        ///////////// เปลี่ยนค่า flag เป็น 3 //////////
        this.tempWorkModel[index] = {
          id: this.tempWorkModel[index].id,
          name: this.tempWorkModel[index].name,
          flag: this.flag
        }

        // if (this.tempWorkModel[index].flag != 0) {
        //   this.tempWorkModel.splice(index, 1);
        // } else {
        //   ///////////// เปลี่ยนค่า flag เป็น 3 //////////
        //   this.tempWorkModel[index] = {
        //     id: this.tempWorkModel[index].id,
        //     name: this.workModelName,
        //     flag: this.flag
        //   }
        // }

      } else {
        //cancel
      }
    });
  }
}
