export default class Utils {
  // static stringToDate(dateString: String): Date {
  //   const [date, time] = dateString.split(" ")
  //   const [d, m, y] = date.split("/")
  //   const [hh, mm] = time.split(":");

  //   var dateObject = new Date(
  //      parseInt(y),
  //      parseInt(m),
  //      parseInt(d),
  //      parseInt(hh),
  //      parseInt(mm),
  //      0);

  //      console.log(dateObject);

  //   return dateObject;
  // }

  static stringToDate(dateString: String): any {
    const [d, m, y] = dateString.split("/")

    let dateObj = {
      date: {
        day: parseInt(d),
        month: parseInt(m),
        year: parseInt(y)
      }
    }
    return dateObj;
  }

  static dateTostring(dateObj: any): String {
    const d = dateObj.date.day
    const m = dateObj.date.month
    const y = dateObj.date.year
    let dateString = d + "/" + m + "/" + (y + 543);
    return dateString;
  }

  // static dateTostring(date: any): String {
  //   let dateString = date.getDate() + "/" + (date.getMonth() + 1) + "/" + (date.getFullYear() + 543);
  //   return dateString;
  // }

}
