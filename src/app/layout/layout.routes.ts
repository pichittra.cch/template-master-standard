import { AuthGuard } from './../shared/guard/auth.guard';
import { Routes, RouterModule } from "@angular/router";
import { LayoutComponent } from "./layout.component";

const LAYOUT_ROUTES: Routes = [

	{
		path: "",
		loadChildren: "../pages/authen/login/login.module#LoginModule"
	},
	{
		path: "",
		component: LayoutComponent,
		children: [
      // { path: "", redirectTo: "account-setting", pathMatch: "full" },
			//---------------------------------------------------------->
			//Dashboards
			//---------------------------------------------------------->
			{
        path: "master-setting/job-construction",
        // canActivate: [AuthGuard],
				loadChildren: "../pages/master-setting/job-construction/job-construction-list/job-construction-list.module#JobConstructionListModule"
			},

			{
        path: "master-setting/job-construction/update/:id",
        // canActivate: [AuthGuard],
				loadChildren: "../pages/master-setting/job-construction/job-construction-update/job-construction-update.module#JobConstructionUpdateModule"
			},
			
			{
        path: "master-setting/job-construction/view/:id",
        // canActivate: [AuthGuard],
				loadChildren: "../pages/master-setting/job-construction/job-construction-view/job-construction-view.module#JobConstructionViewModule"
			},

		]
	},

	// 404 Page Not Found
	{ path: "**", redirectTo: "not-found" }
];

export const LayoutRoutes = RouterModule.forChild(LAYOUT_ROUTES);
