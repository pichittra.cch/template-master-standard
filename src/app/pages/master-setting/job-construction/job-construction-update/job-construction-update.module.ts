import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { NgxsModule } from "@ngxs/store";
import { SharedModule } from '../../../../shared/shared.module';
import { JobConstructionUpdateComponent } from './job-construction-update.component';

const ROUTE = [
  { path: "", component: JobConstructionUpdateComponent }
];

@NgModule({
  declarations: [JobConstructionUpdateComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgxsModule.forRoot([
      
    ]),
    RouterModule.forChild(ROUTE)
  ]
})
export class JobConstructionUpdateModule {}
