import {
  Component,
  ViewEncapsulation,
  OnInit,
  Inject,
  ViewChild,
  TemplateRef
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Subject, Observable } from "rxjs/Rx";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from '../../../../shared/services/config/config.service';
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as _ from "lodash";

@Component({
  selector: ".job-construction-view-inner-wrapper",
  templateUrl: "./job-construction-view.component.html",
  styleUrls: ["./job-construction-view.component.scss"],
  encapsulation: ViewEncapsulation.Emulated
})

export class JobConstructionViewComponent implements OnInit {

  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  data = {
    constructionGroupId: "",
    constructionGroupName: "",
    constructionGroupCode: '',
    code: '',
    accountCode: "",
    name: "",
    workOrderType: "",
    cert: false,
    detail: '',
    active: false,
    jobStyleList: []
  };

  id: string;

  ValidateWorkModel: any = {
    result: true,
    errMsg: ""
  };
  drpConstructionGroup: any;

  //////////////////Modal /////////////////////
  @ViewChild("modalWorkModel")
  public modalWorkModel: TemplateRef<any>;
  modalReferenceWorkModel: NgbModalRef;
  ///////////////// End Modal ////////////////////

  /* paggine */
  maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  maxSize: number = this.config.paging.MAXSIZE;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store,
    public config: ConfigService,
    private modalService: NgbModal,
  ) { }



  ngOnInit() {

    this.id = this.route.snapshot.params["id"] || "0";
    this.getDrpConstructionGroupList()

  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  getDrpConstructionGroupList() {

    // this.store.dispatch(new GetDrpConstructionGroup())
    //   .pipe(
    //     finalize(() => {
    //       this.loading = false;
    //     }),
    //     takeUntil(this.unsubscribeAll)
    //   )
    //   .subscribe(
    //     value => {
    //       this.drpConstructionGroup = value.jobConstructionState.ResultData.result;
          
    //       if (this.id != '0') {
    //         this.getData()
    //       }

    //     },
    //     error => {
    //       console.log("Error:", error)
    //     }
    //   );
  }

  getData() {
    // const data = {
    //   jobId: this.id,
    // }

    // this.loading = true;
    // this.store.dispatch(new GetJobConstructionData(data))
    //   .pipe(
    //     finalize(() => {
    //       this.loading = false;
    //     }),
    //     takeUntil(this.unsubscribeAll)
    //   )
    //   .subscribe(
    //     value => {
    //       this.data = value.jobConstructionState.ResultData.result

    //       this.drpConstructionGroup.filter(item => {
    //         /////////////// หาชื่อกลุ่มงาน เพื่อเอารหัสกลุ่มงาน /////////////////
    //         if (this.data.constructionGroupId == item.id) {
    //           return this.data.constructionGroupName = item.name , this.data.constructionGroupCode = item.code
    //         }
    //       })
    //     },
    //     error => {
    //       console.log("Error:", error)
    //     }
    //   );
  }
}
