import {
  Component,
  ViewEncapsulation,
  OnInit,
  Inject,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import { ConfigService } from '../../../../shared/services/config/config.service';
import Swal from 'sweetalert2';

@Component({
  selector: ".job-construction-list-inner-wrapper",
  templateUrl: "./job-construction-list.component.html",
  styleUrls: ["./job-construction-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated
})

export class JobConstructionListComponent implements OnInit {

  /////////////////// Config ///////////////////
  allRowsSelected: boolean;
  selectedCheck = 0;
  isSearchActive: boolean = false;
  ///////////////// End Config /////////////////


  ///////////////// Variable /////////////////
  public model: any = {};
  public loading = false;
  private unsubscribeAll = new Subject();
  /////////////// End Variable ////////////////

  //////////////// Value /////////////////////
  public dataList: any;
  public dataListTemp: any[] = []
  public drpConstructionGroup: any;
  public searchValue = {
    jobCode: "",
    jobName: "",
    constructionGroupId: "",
    active: "true",
  }
  public checkAll: any;

  ///////////////// End Value ///////////////////

  ///////////////////// Pagine ///////////////////
  public resultPage: any = {
    pageNo: 1,
    totalItem: 0,
    itemPerPage: 0,
    collectionSize: 0
  };
  public maxitemPerPage: number = this.config.paging.ITEMPERPAGE;
  public maxSize: number = this.config.paging.MAXSIZE;
  public totalItemDataList: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store,
    public config: ConfigService,
  ) { }


  ngOnInit() {
    this.getDrpConstructionGroupList()
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public getDrpConstructionGroupList() {
    this.drpConstructionGroup = [
      {
        id: '1',
        name: 'Finishing 1'
      },
      {
        id: '2',
        name: 'Finishing 2'
      },
      {
        id: '3',
        name: 'Finishing 3'
      }
    ]

    this.getList(1)
    // this.store.dispatch(new GetDrpConstructionGroup())
    //   .pipe(
    //     finalize(() => {
    //       this.loading = false;
    //     }),
    //     takeUntil(this.unsubscribeAll)
    //   )
    //   .subscribe(
    //     value => {
    //       this.drpConstructionGroup = value.jobConstructionState.ResultData.result;
    //     },
    //     error => {
    //       this.loading = false
    //     }
    //   );
  }

  public getList(page) {
    this.dataList = [
      {
        id: '1',
        code: '	10-FD-01',
        name: '01-งานวางหมุด',
        constructionGroupName: 'งานโครงสร้าง',
        active: true,
        createdDate: '23/09/2562',
      },
      {
        id: '2',
        code: '	10-FD-02',
        name: '01-งานเสาเข็มตอก',
        constructionGroupName: 'งานโครงสร้าง',
        active: true,
        createdDate: '29/06/2562',
      },
      {
        id: '1',
        code: '	10-FD-04',
        name: '02-งานแคปหัวเข็ม',
        constructionGroupName: 'งานโครงสร้าง',
        active: true,
        createdDate: '23/09/2562',
      },
    ]
    // const data = {
    //   page: page,
    //   itemPerPage: this.config.paging.ITEMPERPAGE,
    //   orderBy: "code",
    //   sort: "asc",
    //   jobCode: this.searchValue.jobCode,
    //   jobName: this.searchValue.jobName,
    //   constructionGroupId: this.searchValue.constructionGroupId,
    // }

    // this.loading = true;
    // this.resultPage.pageNo = page
    // this.store.dispatch(new GetJobConstructionList(data))
    //   .pipe(
    //     finalize(() => {
    //       this.loading = false;
    //     }),
    //     takeUntil(this.unsubscribeAll)
    //   )
    //   .subscribe(
    //     value => {

    //       let CountCheckTrue = 0;
    //       value.jobConstructionState.ResultData.result.map(item => {
    //         let dataList = this.dataListTemp.find(temp => item.id == temp.id);
    //         if (dataList) {
    //           item.selected = true;
    //           CountCheckTrue++;
    //         } else {
    //           item.selected = false;
    //         }
    //         return item;
    //       });

    //       if (CountCheckTrue == value.jobConstructionState.ResultData.result.length) {
    //         this.allRowsSelected = true;
    //       } else {
    //         this.allRowsSelected = false;
    //       }

    //       this.dataList = value.jobConstructionState.ResultData.result;
    //       this.totalItemDataList = value.jobConstructionState.ResultData.resultPage.totalItem;

    //       if (value.jobConstructionState.ResultData.resultPage.totalItem == this.dataListTemp.length) {
    //         this.allRowsSelected = true;
    //       }

    //       this.resultPage.totalItem = value.jobConstructionState.ResultData.resultPage.totalItem;
    //       if (this.totalItemDataList < this.selectedCheck) {
    //         this.selectedCheck = value.jobConstructionState.ResultData.resultPage.totalItem;
    //       } else {
    //         this.selectedCheck = this.dataListTemp.length;
    //       }

    //       // if ((this.selectedCheck == value.jobConstructionState.ResultData.result.length) && this.selectedCheck > 0) {
    //       //   this.checkAll = true;
    //       // } else {
    //       //   this.checkAll = false;
    //       // }
    //     },
    //     error => {
    //       this.loading = false
    //     }
    //   );

  }

  public delete(item, mes) {
    Swal({
      title: "คุณต้องการลบใช่หรือไม่",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: 'btn width-button',
      cancelButtonClass: 'btn btn-danger width-button',
      cancelButtonColor: '#d33',
      confirmButtonText: "บันทึก",
      cancelButtonText: "ยกเลิก"
    }).then((res) => {
      if (res.value) {
        let dataTemp = [];

        if (mes == 'data') {
          this.dataListTemp.push(item.id)
          dataTemp = item.id
        } else {
          this.dataListTemp.map(item => {
            return dataTemp.push(item.id)
          })

        }

      } else {
        //cancel
      }
    });
  }

  public view(id) {
    this.router.navigate([`/master-setting/job-construction/view/`, id]);
  }

  public selectAll(event) {
    this.allRowsSelected = event;
    if (event) {
      this.dataList.map(val => {
        val.selected = true;

        let hasData = this.dataListTemp.find(dataList => {
          return dataList.id == val.id;
        });

        if (!hasData) {
          return this.dataListTemp.push(val);
        }
      });

    } else {
      this.dataListTemp = this.dataListTemp.reduce((result, val) => {
        let hasData = this.dataList.find(data => {
          return data.id == val.id;
        });

        if (!hasData) {
          result = [...result, val];
        }

        return result;
      }, []);

      this.dataList.map(val => {
        val.selected = false;
        return val;
      });
    }
    this.selectedCheck = this.dataListTemp.length;
  }

  public countselectedCheck(event, item) {
    this.dataListTemp = this.dataListTemp.filter(
      temp => temp.id != item.id
    );
    if (event) {
      this.selectedCheck++;
      this.dataListTemp.push(item);
    } else {
      this.selectedCheck--;
    }

    let CountCheckTrue = 0;
    this.dataList.map(item => {
      let dataList = this.dataListTemp.find(temp => item.id == temp.id);

      if (dataList) {
        item.selected = true;
        CountCheckTrue++;
      } else {
        item.selected = false;
        CountCheckTrue--;
      }
      return item;
    });

    if (CountCheckTrue == this.dataList.length) {
      this.allRowsSelected = true;
    } else {
      this.allRowsSelected = false;
    }
  }

  public searchActive() {
    if (this.isSearchActive == false) {
      this.isSearchActive = true;
    } else {
      this.isSearchActive = false;
    }
  }
  public triggerClose(event) {
    this.searchValue = {
      jobCode: "",
      jobName: "",
      constructionGroupId: "",
      active: "true",
    }
    this.getList(1)
    this.isSearchActive = !this.isSearchActive;
  }
}
