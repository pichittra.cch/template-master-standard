import {
	Component,
	OnInit,
	HostListener,
	ViewEncapsulation,
	Input
} from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
import { CookieService } from "ngx-cookie-service";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";

@Component({
	selector: "app-sidebar",
	templateUrl: "./left-sidebar.component.html",
	styleUrls: ["./left-sidebar.component.scss"],
	encapsulation: ViewEncapsulation.Emulated
})
export class LeftSidebarComponent implements OnInit {
	
	constructor(
		private cookieService: CookieService,
	) { }

	ngOnInit() {
		// this.getProfile();
	}

	public config: PerfectScrollbarConfigInterface = {};
	
	scrollbarAutoHide = true
	scrollbarClass = "mainBar"
	@Input("profile") profile;
	@Input("menus") menuList;

	private getProfile(){
		// const jwt = new JwtHelperService();
		// const token = this.cookieService.get("ba_act");
		// const decodedToken = jwt.decodeToken(token);
		
	}


	@HostListener("window:resize")
	public onWindowResize(): void { }
}
