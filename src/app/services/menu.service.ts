import { ResultData } from './../models/result.data';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { RestUrlConfigService, IMenu } from '../shared/services/config/rest-url.config.service';


@Injectable({
  providedIn: 'root'
})
export class MenuService {
  constructor(
    private http: HttpClient,
    private restUrl: RestUrlConfigService
  ) { 
    this.urlServ = this.restUrl.menu;
  }

  public urlServ: IMenu;

  getMenu = (): Observable<any> => {
    return this.http
      .get<ResultData>(this.urlServ.getMenu);
  };
}
