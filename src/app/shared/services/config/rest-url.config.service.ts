import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class RestUrlConfigService {

    private BASE_URL = "api";
    public auth: IAuth;
    public user: IUser;
    public menu: IMenu;

    public content: IContent;

    constructor() {
        
        this.auth = {
            getHealth: this.BASE_URL+"/authen-service/health",
            authLogin: this.BASE_URL+"/auth",
            getAccessToken: this.BASE_URL+"/auth/access-token"
        }

        this.content = {
            getContent: "https://api.github.com/users/mralexgray/repos",
            postContent: "https://www.mocky.io/v2/5185415ba171ea3a00704eed"
        }

        this.user = {
            getMe: this.BASE_URL+"/user/me"
        }

        this.menu = {
            getMenu: this.BASE_URL+"/menu"
        }

    }
}

export interface IAuth {
    getHealth: string;
    authLogin: string;
    getAccessToken: string;
    
}

export interface IContent {
    getContent: string;
    postContent: string;
}

export interface IUser {
    getMe: string;
}

export interface IMenu {
    getMenu: string;
}
