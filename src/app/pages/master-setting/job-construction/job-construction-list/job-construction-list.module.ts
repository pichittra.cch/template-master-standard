import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { NgxsModule } from "@ngxs/store";
import { SharedModule } from '../../../../shared/shared.module';
import { JobConstructionListComponent } from "./job-construction-list.component";

const ROUTE = [
  { path: "", component: JobConstructionListComponent },
];

@NgModule({
  declarations: [JobConstructionListComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgxsModule.forRoot([
      
    ]),
    RouterModule.forChild(ROUTE)
  ]
})
export class JobConstructionListModule {}
