import { GetMenu } from './../actions/menu.actions';
import { ResultData } from './../models/result.data';
import { State, Action, StateContext } from '@ngxs/store';
import { tap } from "rxjs/operators";
import { MenuService } from '../services/menu.service';


export interface MenuStateModel {
    menu: ResultData
}

@State<MenuStateModel>({
    name: "menuState"
})


export class MenuState {

    constructor(public service: MenuService) {}

    @Action(GetMenu)
    getMenu(ctx: StateContext<MenuStateModel>) {
        const state = ctx.getState();
        
        return this.service.getMenu().pipe(
            tap(value => {
                ctx.patchState({ 
                    ...state,
                    menu: value
                });
            })
        )
    }
}