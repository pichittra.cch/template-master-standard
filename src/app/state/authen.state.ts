import { AuthLogin, GetAccessToken, GetHealth } from './../actions/authen.actions';
import { ResultData } from './../models/result.data';
import { State, Action, StateContext } from '@ngxs/store';
import { tap } from "rxjs/operators";
import { AuthenService } from '../services/authen.service';


export interface AuthenStateModel {
    health: ResultData
    authLogin: ResultData;
    accessToken: ResultData;
}

@State<AuthenStateModel>({
    name: "authenState"
})


export class AuthenState {

    constructor(public service: AuthenService) {}

    @Action(GetHealth)
    getHealth(ctx: StateContext<AuthenStateModel>) {
        const state = ctx.getState();
        
        return this.service.getHealth().pipe(
            tap(value => {
                ctx.patchState({ 
                    ...state,
                    health: value
                 });
            })
        )
    }

    @Action(AuthLogin)
    authLogin(ctx: StateContext<AuthenStateModel>, payload: any) {
        const state = ctx.getState();
        
        return this.service.authLogin(payload).pipe(
            tap(value => {
                ctx.patchState({ 
                    ...state,
                    authLogin: {...value}
                 });
            })
        )
    }

    @Action(GetAccessToken)
    getAccessToken(ctx: StateContext<AuthenStateModel>, payload: any) {
        const state = ctx.getState();
        
        return this.service.getAccessToken(payload).pipe(
            tap(value => {
                ctx.patchState({ 
                    ...state,
                    accessToken: {...value}
                 });
            })
        )
    }
}