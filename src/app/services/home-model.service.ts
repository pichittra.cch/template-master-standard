import { IContent } from './../shared/services/config/rest-url.config.service';
import { ResultData } from './../models/result.data';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { RestUrlConfigService } from '../shared/services/config/rest-url.config.service';


@Injectable({
  providedIn: 'root'
})
export class HomeModelService {
  constructor(
    private http: HttpClient,
    private restUrl: RestUrlConfigService
  ) { 
    this.urlServ = this.restUrl.content;
  }

  public urlServ: IContent;

  // upload = (params): Observable<any> => {
  //   let data: File[] = params.payload

  //   const formData = new FormData();

  //   data.forEach((value) => {
  //     formData.append('files', value); // ชื่อ (files) คือ name ที่ฝั่ง service ต้องการ
  //   });


  //   return this.http
  //     .post<ResultData>(this.urlServ.upload, formData, {
  //       reportProgress: true
  //     });

  // };

 
}
