import {
    Component,
    ViewEncapsulation,
    OnInit,
    Inject,
} from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import swal from 'sweetalert2';
import { SESSION_STORAGE, LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { Store } from "@ngxs/store";
import { Subject } from "rxjs";
import { AuthLogin } from "../../../actions/authen.actions";
import { finalize, takeUntil } from "rxjs/operators";
import { CookieService } from "ngx-cookie-service";
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
    selector: ".content_inner_wrapper",
    templateUrl: "./logout.component.html",
    styleUrls: ["./logout.component.scss"],
    encapsulation: ViewEncapsulation.Emulated
})
export class LogoutComponent implements OnInit {
    constructor(
        @Inject(SESSION_STORAGE) private sessionstorage: StorageService,
        @Inject(LOCAL_STORAGE) private localstorage: StorageService,
        private router: Router,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        private store: Store
      ) {}

    ngOnInit() {
        this.logout();
    }

    ngOnDestroy() {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
    }

    /////////////////// Config ///////////////////
 
    ///////////////// End Config /////////////////
    ///////////////// Variable /////////////////
    public model: any = {};;
    public loading = false;
    private unsubscribeAll = new Subject();
    /////////////// End Variable ////////////////

 
    public logout() {
        // const data = {
        //     grantType: "LOGIN",
        //     ...this.model
        // }
    
        // this.loading = true;
        // this.store.dispatch(new AuthLogin(data))
        // .pipe(
        //   finalize(() => {
        //     this.loading = false;
        //   }),
        //   takeUntil(this.unsubscribeAll)
        // )
        // .subscribe(
        //   value => {
    
        //     if(value.authenState.authLogin.status === 200){
        //         this.setSession(value.authenState.authLogin.result);
        //     }
            
        //   },
        //   error => {
        //     console.log("Error:", error)
        //   }
        // );

        this.localstorage.clear();
        this.cookieService.deleteAll();
        this.router.navigate(['/login'])
    
    }

    private setSession(result) {
        //https://github.com/auth0/angular2-jwt
        // let now = new Date();
        // let minutes = result.expiresIn;
        // now.setTime(now.getTime() + (minutes * 1000));

        const jwt = new JwtHelperService();

        const expirationDateRefreshToken = jwt.getTokenExpirationDate(result.refreshToken);
        const expirationDateAccessToken = jwt.getTokenExpirationDate(result.accessToken);

        this.cookieService.set( 'ilg', 'true', expirationDateRefreshToken, "/" );
        this.cookieService.set( 'act', result.accessToken, expirationDateAccessToken, "/" );
        this.cookieService.set( 'rft', result.refreshToken, expirationDateRefreshToken, "/" );
        this.localstorage.set('tkt', result.tokenType)
        this.localstorage.set('uid', result.userId)
      
        this.router.navigate(['/']);

    }

}
