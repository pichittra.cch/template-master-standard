export interface ResultData {
    result: any;
    resultPage: any;
    status: number;
    message: string;
    token: string;
  }