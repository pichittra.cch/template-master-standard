import { LoadingComponent } from "./loading.component";
import { CommonModule } from "@angular/common";
import { NgModule } from '@angular/core';
import { NgxLoadingModule } from "ngx-loading";

@NgModule({
	declarations: [LoadingComponent],
	providers: [],
	imports: [
		CommonModule,
		NgxLoadingModule
	],
	exports: [
		LoadingComponent
	]
})
export class LoadingModule {}