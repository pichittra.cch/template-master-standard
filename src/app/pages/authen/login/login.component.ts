import {
    Component,
    ViewEncapsulation,
    OnInit,
    Inject,
} from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import swal from 'sweetalert2';
import { SESSION_STORAGE, LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { Store } from "@ngxs/store";
import { Subject } from "rxjs";
import { AuthLogin } from "../../../actions/authen.actions";
import { finalize, takeUntil } from "rxjs/operators";
import { CookieService } from "ngx-cookie-service";
import { JwtHelperService } from '@auth0/angular-jwt';
import sha256 from 'crypto-js/sha256';
import Base64 from 'crypto-js/enc-base64';

@Component({
    selector: ".content_inner_wrapper",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
    encapsulation: ViewEncapsulation.Emulated
})
export class LoginComponent implements OnInit {
    constructor(
        @Inject(SESSION_STORAGE) private sessionstorage: StorageService,
        @Inject(LOCAL_STORAGE) private localstorage: StorageService,
        private router: Router,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        private store: Store
      ) {}

    ngOnInit() {
       // this.checkLogin();
    }

    ngOnDestroy() {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
    }

    /////////////////// Config ///////////////////

    ///////////////// End Config /////////////////
    ///////////////// Variable /////////////////
    public username: string = "";
    public password: string = "";
    public loading = false;
    public showLogin = true;
    private unsubscribeAll = new Subject();
    /////////////// End Variable ////////////////
    public submitValue: any = {
      result: true,
      resultValue: ""
    };

    public validateFromgruop() {
      this.submitValue.result = false;

      if(this.username == "") {
        this.submitValue.resultValue = "กรุณากรอกผู้ใช้งาน";
      } else if (this.password == "") {
        
        this.submitValue.resultValue = "กรุณากรอกรหัสผ่าน";
      } else {
        this.submitValue.result = true;
        this.onSubmit();
      }
  
    }

    public onSubmit() {
      this.router.navigate(['/master-setting/job-construction']);
        // const data = {
        //     grantType: "LOGIN",
        //     // ...this.model
        //     username: this.model.username,
        //     password: Base64.stringify(sha256(this.model.password))
        // }

        // this.loading = true;
        // this.store.dispatch(new AuthLogin(data))
        // .pipe(
        //   finalize(() => {
        //     this.loading = false;
        //   }),
        //   takeUntil(this.unsubscribeAll)
        // )
        // .subscribe(
        //   value => {

        //     if(value.authenState.authLogin.status === 200){
        //         this.setSession(value.authenState.authLogin.result);
        //     }

        //   },
        //   error => {
        //     console.log("Error:", error)
        //   }
        // );

    }

    // private checkLogin(){
    //     let isLogin = this.cookieService.get( 'ba_ilg');

    //     if (isLogin === "true") {
    //         return this.router.navigate(['/']);
    //     }

    //     this.showLogin = true;
    // }

}
