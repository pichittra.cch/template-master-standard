
import { Router, NavigationEnd} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable, Inject } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from "@angular/common/http";
import { SESSION_STORAGE, LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { filter, pairwise } from 'rxjs/operators';
import * as _ from "lodash";




@Injectable({
    providedIn: 'root'
})
//https://itnext.io/angular-tutorial-implement-refresh-token-with-httpinterceptor-bfa27b966f57
//http://ericsmasal.com/2018/07/02/angular-6-with-jwt-and-refresh-tokens-and-a-little-rxjs-6/
export class SystemInterceptor implements HttpInterceptor  {
    constructor(
        @Inject(SESSION_STORAGE) private sessionstorage: StorageService,
        @Inject(LOCAL_STORAGE) private localstorage: StorageService,
        private router: Router
        
    ) {

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let returnReq: HttpRequest<any>;

        switch(request.method) { 
            case "POST": { 
                returnReq = this.postRequest(request);
                break; 
            } 
            case "GET": { 
                returnReq = this.getRequest(request);
                break; 
            } 
            default: { 
                returnReq = request;
               break; 
            } 
        } 

        return next.handle(returnReq);
    }

    private postRequest = (request: HttpRequest<any>) => {

        let lang = this.localstorage.get("lang");
        if(lang == null || !lang || lang === ""){
            lang = "th";
        }
    
        let customBody = {
            ...request.body,
            "lang": lang
        }

        let header = new HttpHeaders();

        header = header.append('My-Header', 'MyHeaderValue');
        header = header.append('Content-Type', 'application/json;charset=utf8');
    
        request = request.clone({

            headers: header,
            body: customBody

        });

        return request;
    }

    private getRequest = (request: HttpRequest<any>) => {

        let lang = this.localstorage.get("lang");
        if(lang == null || !lang || lang === ""){
            this.localstorage.set("lang", "th")
            lang = "th";
        }

        request = request.clone({

            headers: request.headers.set('My-Header', 'MyHeaderValue'),
            params: request.params.append("lang", lang)

        });

        return request;
    }

}