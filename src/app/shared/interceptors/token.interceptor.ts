import { GetAccessToken } from './../../actions/authen.actions';

import { Router, NavigationEnd} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable, Inject } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent } from "@angular/common/http";
import { SESSION_STORAGE, LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { filter, catchError, switchMap, finalize, take } from 'rxjs/operators';
import * as _ from "lodash";
import { throwError, BehaviorSubject } from 'rxjs';
import { Store } from '@ngxs/store';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})
//https://itnext.io/angular-tutorial-implement-refresh-token-with-httpinterceptor-bfa27b966f57
//http://ericsmasal.com/2018/07/02/angular-6-with-jwt-and-refresh-tokens-and-a-little-rxjs-6/
export class TokenInterceptor implements HttpInterceptor  {
    constructor(
        @Inject(LOCAL_STORAGE) private localstorage: StorageService,
        private cookieService: CookieService,
        private store: Store,
        private router: Router
    ) {

    }

    private isRefreshingToken: boolean = false;
    private tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    intercept(request: HttpRequest<any>, next: HttpHandler) : Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {

        let urlArr = request.url.split("/");

        if(urlArr[0] === "assets") {
            return next.handle(request)
        } else {
            return next.handle(this.addTokenToRequest(request))
                .pipe(
                    catchError(err => {
                        if (err instanceof HttpErrorResponse) {
                            switch ((<HttpErrorResponse>err).status) {
                            case 401:
                                return this.handle401Error(request, next);
                            case 400:
                                return <any>this.router.navigate(['/logout']);
                            }
                        } else {
                            return throwError(err);
                        }
                }));
        }
        
        
        
    }

    private addTokenToRequest(request: HttpRequest<any>) : HttpRequest<any> {
        let token = this.cookieService.get("ba_act");
        let tokenType = this.localstorage.get("ba_tkt");

        return request.clone({ setHeaders: { Authorization: `${tokenType} ${token}`}});
    }
    
    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    
        if(!this.isRefreshingToken) {
            this.isRefreshingToken = true;
    
            // Reset here so that the following requests wait until the token
            // comes back from the refreshToken call.
            this.tokenSubject.next(null);

            const data = {
                grantType: "ACCESS_TOKEN",
                uid: this.localstorage.get('ba_uid'),
                refreshToken: this.cookieService.get('ba_rft')
            }
    
            return this.store.dispatch(new GetAccessToken(data))
                .pipe(
                    switchMap((result) => {
                        let data = result.authenState.accessToken;
                        let status = data.status;
                        
                        if(status === 200) {
                            this.setSession(data.result);
                            this.tokenSubject.next(data.result.accessToken);;

                            return next.handle(this.addTokenToRequest(request));
                        }
    
                    return <any>this.router.navigate(['/logout']);
                }),
                catchError(err => {
                    return <any>this.router.navigate(['/logout']);
                }),
                finalize(() => {
                    this.isRefreshingToken = false;
                })
            );
        } else {
          this.isRefreshingToken = false;
    
          return this.tokenSubject
            .pipe(filter(token => token != null),
                take(1),
                switchMap(token => {
                    return next.handle(this.addTokenToRequest(request));
                }
            ));
        }
    }

    private setSession(result) {
        const jwt = new JwtHelperService();

        const expirationDateAccessToken = jwt.getTokenExpirationDate(result.accessToken);

        this.cookieService.set( 'ba_act', result.accessToken, expirationDateAccessToken, "/" );
        this.localstorage.set('ba_tkt', result.tokenType)
        this.localstorage.set('ba_uid', result.uid)

    }

}