import { AuthLogin, GetAccessToken, GetHealth } from './../actions/authen.actions';
import { ResultData } from './../models/result.data';
import { State, Action, StateContext } from '@ngxs/store';
import { tap } from "rxjs/operators";
import { ContentService } from '../services/content.service';
import { GetContent, PostContent } from '../actions/content.actions';


export interface ContentStateModel {
    testContent: ResultData;
    res: ResultData;
}

@State<ContentStateModel>({
    name: "contentState"
})


export class ContentState {

    constructor(public service: ContentService) {}

    @Action(GetContent)
    getContent(ctx: StateContext<ContentStateModel>) {
        const state = ctx.getState();
        
        return this.service.getContent().pipe(
            tap(value => {
                ctx.patchState({ 
                    ...state,
                    testContent: value
                 });
            })
        )
    }

    @Action(PostContent)
    postContent(ctx: StateContext<ContentStateModel>, payload: any) {
        const state = ctx.getState();
        
        return this.service.postContent(payload).pipe(
            tap(value => {
                ctx.patchState({ 
                    ...state,
                    res: {...value}
                 });
            })
        )
    }

    // @Action(GetAccessToken)
    // getAccessToken(ctx: StateContext<ContentStateModel>, payload: any) {
    //     const state = ctx.getState();
        
    //     return this.service.getAccessToken(payload).pipe(
    //         tap(value => {
    //             ctx.patchState({ 
    //                 ...state,
    //                 accessToken: {...value}
    //              });
    //         })
    //     )
    // }
}